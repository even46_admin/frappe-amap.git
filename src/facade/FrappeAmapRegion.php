<?php

namespace frappe\amap\facade;

use think\Facade;

/**
 * Class FrappeAmapRegion
 * @package frappe\facade
 * @mixin \frappe\amap\region\FrappeAmapRegion
 */
class FrappeAmapRegion extends Facade
{
    protected static function getFacadeClass()
    {
        return \frappe\amap\region\FrappeAmapRegion::class;
    }
}