<?php
// +----------------------------------------------------------------------
// | 高德地图接口
// +----------------------------------------------------------------------
// | 日期 2024/03/22 18:23 下午
// +----------------------------------------------------------------------
// | 开发者 Even <yinxu46@qq.com>
// +----------------------------------------------------------------------
// | 版权所有 2023~2024 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\amap\region;

use Exception;
use InvalidArgumentException;
use Throwable;

/**
 * 行政区域
 */
class RegionClient
{
    /**
     * 接口地址
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var bool
     */
    protected $cache;

    /**
     * @param array $options
     */
    function __construct(array $options)
    {
        if (empty($options['key'])) {
            throw new InvalidArgumentException("Missing Amap Config -- [Key]");
        }
        if (empty($options['url'])) {
            throw new InvalidArgumentException("Missing Amap Config -- [Url]");
        }
        $this->key = $options['key'];
        $this->url = $options['url'];
        $this->cache = $options['cache'] ?? true;
    }

    /**
     * @throws Exception
     */
    public function queryRegion()
    {
        $org_regions = $this->request();
        if (!$org_regions) throw new \Exception('Amap Api No Data');
        # 中国
        $root = $org_regions[0];
        $root_code = $root['adcode'];
        $regions = [];
        foreach ($root['districts'] ?? [] as $district) {
            $regions[] = $this->convert($district, $root_code);
        }
        if (is_array($regions) && !empty($regions)) return $regions;
        return [];
    }

    protected function convert(array $region, $pid): array
    {
        $new_region = [
            'id' => $region['adcode'],
            'pid' => $pid,
            'name' => $region['name'],
            'center' => $region['center'],
            'level' => $region['level'],
        ];
        $districts = $region['districts'] ?? [];
        if ($districts) {
            $children = [];
            foreach ($districts as $district) {
                $children[] = $this->convert($district, $region['adcode']);
            }
            $new_region['children'] = $children;
        }
        return $new_region;
    }

    /**
     * @throws Exception
     */
    public function request($subdistrict = 3): array
    {
        $url = "{$this->url}?key={$this->key}&subdistrict=$subdistrict";
        $res = $this->doRequest($url);
        if (empty($res['districts']) || !is_array($res['districts'])) {
            return [];
        }
        return $res['districts'];
    }

    /**
     * 发送请求
     * @param string $url
     * @return mixed
     * @throws Exception
     * @author yinxu
     * @date 2024/3/23 00:05:18
     */
    private function doRequest(string $url)
    {
        try {
            $content = $this->fetchContent($url);
            return json_decode($content, true);
        } catch (Throwable $t) {
            throw new Exception($t->getMessage(), $t->getCode());
        }
    }

    /**
     * curl 请求
     * @param string $url
     * @return bool|mixed|string
     * @author yinxu
     * @date 2024/3/22 18:33:57
     */
    private function fetchContent(string $url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "x-sdk-client" => "php/2.0.0"
        ));

        if (substr($url, 0, 5) == 'https') {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        $rtn = curl_exec($ch);

        if ($rtn === false) {
            trigger_error("[CURL_" . curl_errno($ch) . "]: " . curl_error($ch), E_USER_ERROR);
        }

        curl_close($ch);

        return $rtn;
    }
}