<?php

namespace frappe\amap\region;

class FrappeAmapTool
{
    /**
     * 查询指定节点
     * @param array $tree
     * @param $id
     * @return mixed|null
     * @author yinxu
     * @date 2024/3/31 00:00:48
     */
    public static function getNodeById(array $tree, $id)
    {
        foreach ($tree as $item) {
            if ($item['id'] == $id) {
                return $item;
            }
            if (!empty($item['children'])) {
                $sub_item = self::getNodeById($item['children'], $id);
                if ($sub_item) return $sub_item;
            }
        }
        return null;
    }

    /**
     * 获取当前节点包含全部子节点的ID合集
     * @param array $tree
     * @return array
     * @author yinxu
     * @date 2024/3/31 00:03:14
     */
    public static function getNodeIds(array $tree, bool $hasCurrent = true): array
    {
        if (isset($tree['id'])) {
            $trees = $hasCurrent ? [$tree] : ($tree['children'] ?? []);
        } else {
            $trees = $tree;
        }
        $ids = [];
        foreach ($trees as $item) {
            $ids[] = $item['id'];
            if (!empty($item['children'])) {
                $sub_ids = self::getNodeIds($item['children']);
                if (!empty($sub_ids)) {
                    $ids = array_merge($ids, $sub_ids);
                }
            }
        }
        return $ids;
    }
}