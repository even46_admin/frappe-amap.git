<?php

namespace frappe\amap\region;

use think\Config;

class FrappeAmapRegion
{
    /**
     * @var Config
     */
    protected $config;
    /**
     * @var RegionClient
     */
    protected $client;
    /**
     * @var array 原始数据
     */
    protected $org_regions = [];
    /**
     * @var string Root编码
     */
    protected $root_code = "100000";
    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $key = $config->get('amap.key', '');
        $url = $config->get('amap.region.url', '');
        $this->client = new RegionClient(['key' => $key, 'url' => $url]);
    }

    /**
     * 加载远程数据
     * @throws \Exception
     */
    protected function load()
    {
        $this->org_regions = $this->client->queryRegion();
    }

    /**
     * 获取全部区域 - 平级
     * @return array
     * @throws \Exception
     * @author yinxu
     * @date 2024/3/25 12:03:28
     */
    public function regions(): array
    {
        $this->load();
        $regions = [];
        foreach ($this->org_regions ?? [] as $region) {
            $regions = array_merge($regions, $this->getChildren($region));
        }
        return $regions;
    }

    /**
     * 获取Tree结构数据
     * @return array
     * @throws \Exception
     * @author yinxu
     * @date 2024/4/2 14:30:22
     */
    public function treeRegions(): array
    {
        $this->load();
        return $this->org_regions ?? [];
    }

    # 获取当前下级区域Code集合
    public function getSubRegionCodesByCode(string $code): array
    {
        $tree = FrappeAmapTool::getNodeById($this->org_regions, $code);
        if (!$tree) return [];
        return FrappeAmapTool::getNodeIds($tree);
    }

    protected function getChildren(array $region = []) :array
    {
        $children = $region['children'] ?? [];
        unset($region['children']);
        $regions[] = $region;
        foreach ($children as $child) {
            $regions = array_merge($regions, $this->getChildren($child));
        }
        return $regions;
    }
}